#![feature(proc_macro_hygiene, decl_macro)]
#![feature(plugin)]

#[macro_use]
extern crate rocket;

 extern crate rocket_contrib;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate diesel;


#[macro_use] extern crate serde_json;
extern crate serde;

extern crate rustc_serialize;
extern crate crypto;
extern crate jwt;

extern crate dotenv;

mod db;
mod user;
mod hero;


fn main() {
    let mut rocket = rocket::ignite()
        .manage(db::connect());
    rocket = user::mount(rocket);
    rocket = hero::mount(rocket);
    rocket.launch();
}