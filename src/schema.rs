table! {
    heroes (id) {
        id -> Integer,
        nama -> Varchar,
        identitas -> Varchar,
        hometown -> Varchar,
        age -> Integer,
    }
}

table! {
    users (id) {
        id -> Integer,
        nama -> Varchar,
        passwords -> Varchar,
    }
}

allow_tables_to_appear_in_same_query!(
    heroes,
    users,
);
